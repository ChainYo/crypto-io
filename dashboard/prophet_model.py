import pandas as pd

from streamlit import cache
from neuralprophet import NeuralProphet

@cache(ttl=60)
def get_predictions(dataframe: pd.DataFrame):
    """
    Get predictions for a dataframe.

    Args:
        df (pandas.DataFrame): Dataframe with the time series.

    Returns:
        pandas.DataFrame: Dataframe with the predictions.
    """

    m = NeuralProphet()

    metrics = m.fit(dataframe, validate_each_epoch=True, valid_p=0.2, freq="D")
    future = m.make_future_dataframe(dataframe, periods=7)

    forecast = m.predict(future)
    forecast.drop(['y', 'residual1', 'trend'], axis=1, inplace=True)
    try:
        forecast.drop(['season_yearly'], axis=1, inplace=True)
        forecast.drop(['season_weekly'], axis=1, inplace=True)
    except:
        pass

    forecast.rename(columns={'ds': 'timestamps', 'yhat1': 'prices'}, inplace=True)

    return forecast
