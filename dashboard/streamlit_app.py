import pandas as pd
import streamlit as st
import plotly.graph_objects as go

from annotated_text import annotated_text
from plotly.subplots import make_subplots

from helpers.currencies_vs import currencies_vs

from utils import cache_all_crypto, cache_all_currencies
from utils import cache_coin_data, cache_coin_data_history
from utils import convert_timestamp, to_prophet_format

from database import DataBase
from prophet_model import get_predictions

# Layout
st.set_page_config(
    layout="centered",
    page_title='Crypto.io',
    page_icon=":moneybag:")
st.title("Cryptocurrency Forecasting Dashboard")
st.markdown("""
    ![Version](https://img.shields.io/badge/Version-0.1-green)
    ![Python](https://img.shields.io/badge/Python-3.9.6-blue)
    ![NeuralProphet](https://img.shields.io/badge/Tensorflow-2.6.2-yellow)
    ![Streamlit](https://img.shields.io/badge/Streamlit-1.0.0-yellow)
    ![License](https://img.shields.io/badge/License-Apache_2.0-orange)
""")
with st.expander("About"):
    st.markdown("""
        This app is a forecasting dashboard for your cryptocurrency portfolio.  
        I made this for my own portfolio tracking purposes.  
        I love crypto and love building apps. I needed also a tool 
        that could help me, and I wanted to see if I could build one.  
        Some IA features were added to the app I never found anywhere
        else.    
        I hope you enjoy!
    """)

def update_default_currency(username:str, currency:str):
    DataBase.set_default_currency(username, currency)
    st.session_state['default_currency'] = DataBase.get_default_currency(username)

def update_user_targets(username:str, coin:str, event:str):
    if event == 'add':
        DataBase.add_to_user_targets(username, coin)
    elif event == 'remove':
        DataBase.remove_from_user_targets(username, coin)
    st.session_state['targets'] = DataBase.get_user_targets(username) 

# Session state variables
if 'logged' not in st.session_state:
    st.session_state['logged'] = False

if st.session_state['logged'] == False:
    # Login feature
    login_container = st.empty()
    login_container.markdown("""
        <center><h2>Enter your credentials</h2></center>
        <br>
        <center><p>You need to check <bold><em>Register me</em></bold> if it is the first time 
        you connect to the application.</p></center>
    """, unsafe_allow_html=True)
    with login_container.form(key='login', clear_on_submit=True):
        input_user = st.text_input('👤 Username', '')
        input_pass = st.text_input('🔑 Password', '', type='password')
        register = st.checkbox('Register me', value=False)
        submit = st.form_submit_button('Login')

    if submit:
        response = DataBase.authentication(input_user, input_pass)
        if register:
            exist = DataBase.check_username(input_user)
            if exist == False:
                DataBase.create_user(input_user, input_pass)
                DataBase.set_default_currency(input_user, 'USD')
                st.success("You are now registered. You can now login.")
            elif response == True:
                st.error("This username is already taken. Please choose another one.")
            else:
                st.error(f"{exist}")
        else:
            if response == False:
                st.error("Wrong username or password.")
            elif response == True:
                if 'user' not in st.session_state:
                    st.session_state['user'] = input_user
                st.session_state['default_currency'] = DataBase.get_default_currency(st.session_state['user'])
                st.session_state['targets'] = DataBase.get_user_targets(st.session_state['user'])
                st.session_state['logged'] = True
                st.success(f"You are now logged in. Welcome back {input_user}")
                login_container.empty()


if st.session_state['logged'] == True:
    # Sidebar
    st.sidebar.title("Dashboard Panel")
    with st.sidebar.container():
        cols = st.columns([4, 1])
        with cols[0]:
            crypto_track = st.selectbox(
                "# Choose one crypto to compare", 
                cache_all_crypto(),
                index=511,
                help="You can filter all currencies by typing in the selectbox your choice.")
        with cols[1]:
            st.text("") # For layout purposes
            st.text("") # For layout purposes
            add_btn = st.button(
                label="+", 
                key="add_coin", 
                on_click=DataBase.add_to_user_targets,
                args=(st.session_state["user"], crypto_track, 'add'),
                help="Click this button to add selected coin to your targets.")
        currency_track = st.sidebar.selectbox(
            "# Choose with which currency to compare",
            cache_all_currencies(),
            index=st.session_state['default_currency'][0]-1,
            help="If you need to see the full currency name, check the window below.")
    with st.sidebar.container():
        col1, col2 = st.columns(2)
        with col1:
            st.button(
                label="Set default", 
                key="set_default", 
                on_click=update_default_currency,
                args=(st.session_state['user'], currency_track),
                help="Click this button to define current currency as your default.")
        with col2:
            annotated_text((f"{st.session_state['default_currency'][1]}", "default", "#fea"))
    with st.sidebar.expander("currencies description"):
        st.markdown(currencies_vs)
    forecasting = st.sidebar.checkbox(
        "Forecasting",
        value=False,
        help="Enable or disable forecasting.")
    st.sidebar.title("Targeted Coins")
    targets_placeholder = st.sidebar.empty()
    with targets_placeholder.container():
        for coin in st.session_state['targets']:
            cols = st.columns([4, 1])
            with cols[0]:
                st.text(coin)
            with cols[1]:
                st.button(
                    label="X", 
                    key=coin, 
                    on_click=update_user_targets,
                    args=(st.session_state["user"], coin, 'remove'),
                    help=f"Click to remove {coin} from your targets.")

    # Dashboard
    data = cache_coin_data(crypto_track.lower())

    if 'error' in data:
        st.error(data['error'])
    else:
        # Logo, name and symbol
        if 'image' in data:
            st.markdown(f"""
                <img style='display:block; margin-right:auto; margin-left:auto' src="{data['image']['small']}"/>
            """, unsafe_allow_html=True)
        st.markdown(f"""
                <h1 style='text-align:center'>{data['name']} ({data['symbol']})</h1>
        """, unsafe_allow_html=True)

        # Price
        if 'market_data' in data:
            st.markdown(f"""
                <h3 style='text-align:center'>🪙 {data['market_data']['current_price'][currency_track.lower()]:,} {currency_track}</h3>
            """, unsafe_allow_html=True)

            # Market cap and volume
            st.markdown(f"""
                <p>Market cap: {data['market_data']['market_cap'][currency_track.lower()]:,} {currency_track}</p>
                <p>24h volume: {data['market_data']['total_volume'][currency_track.lower()]:,} {currency_track}</p>
            """, unsafe_allow_html=True)

        # Plot
        historical = cache_coin_data_history(crypto_track.lower(), currency_track.lower())

        if 'error' in historical:
            st.error(historical['error'])
        else: 
            df = pd.DataFrame(historical['prices'], columns=['timestamps', 'prices'])
            df['market_caps'] = [i[1] for i in historical['market_caps']]
            df['total_volumes'] = [i[1] for i in historical['total_volumes']]
            df['timestamps'] = df['timestamps'].map(convert_timestamp)
            df.dropna(inplace=True)

            fig_main = make_subplots(
                rows=4, cols=2,
                specs=[
                    [{"rowspan":4, "colspan": 2}, None], 
                    [None, None],
                    [None, None],
                    [None, None]
                ],
                vertical_spacing=0.15)

            fig_main.add_trace(go.Scatter(x=df['timestamps'], y=df['prices'], name='Price', mode='lines', fill='tozeroy'), row=1, col=1)
            
            if forecasting:
                forecast = get_predictions(to_prophet_format(df.copy(deep=True)))
                df = pd.concat([df, forecast], ignore_index=True)
                fig_main.add_trace(go.Scatter(x=df['timestamps'], y=df['prices'], name='Forecasting Price', mode='lines', hovertemplate=None), row=1, col=1)

                price, forecasting_price = fig_main['data']
                fig_main.data = forecasting_price, price

            fig_main.update_layout(height=600, title_text="Cryptocurrency Prices History", plot_bgcolor="#d9dbda", hovermode="x")
            fig_main.update_xaxes(title_text="Date", gridcolor="#d9dbda")
            fig_main.update_yaxes(title_text=f"Price ({currency_track})", gridcolor="#d9dbda")

            st.plotly_chart(fig_main, use_container_width=True)
            
            fig_second = make_subplots(
                rows=3, cols=2,
                specs=[
                    [{"rowspan":3}, {"rowspan":3}],
                    [None, None],
                    [None, None]
                ],
                x_title="Date",
                y_title=f"Volume ({currency_track})",
                vertical_spacing=0.15)

            fig_second.add_trace(go.Scatter(x=df['timestamps'], y=df['market_caps'], name='Market cap', mode='lines', fill='tozeroy'), row=1, col=1)
            fig_second.add_trace(go.Scatter(x=df['timestamps'], y=df['total_volumes'], name='Volume', mode='lines', fill='tozeroy'), row=1, col=2)
            
            fig_second.update_layout(height=600, title_text="Cryptocurrency Market Cap and Volume History", plot_bgcolor='#d9dbda', hovermode="x")
            fig_second.update_xaxes(gridcolor="#d9dbda")
            fig_second.update_yaxes(gridcolor="#d9dbda")

            st.plotly_chart(fig_second, use_container_width=True)

# Hide streamlit header/footer
hide_streamlit_style = """
<style>
#MainMenu {visibility: hidden;}
footer {visibility: hidden;}
</style>
"""
st.markdown(hide_streamlit_style, unsafe_allow_html=True)
