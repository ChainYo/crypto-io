import re
import json
import requests
import pandas as pd

from datetime import datetime
from streamlit import cache

@cache
def cache_all_crypto():
    """
    Request all cryptos names listed on CoinGecko API 
    and store it in cache.

    Returns:
        List of crypto names.
    """
    target = "https://api.coingecko.com/api/v3/coins/list?include_platform=false"

    return [i['name'] for i in requests.get(target).json() if confirmed_crypto(i['name'])]

@cache
def cache_all_currencies():
    """
    Request all currencies names listed on CoinGecko API 
    and store it in cache.

    Returns:
        List of currencies names.
    """

    target = "https://api.coingecko.com/api/v3/simple/supported_vs_currencies"
    
    return [i.upper() for i in requests.get(target).json()]

@cache(ttl=30)
def cache_coin_data_history(coin: str, currency: str):
    """
    Request historical data for a coin and a currency
    and store it in cache.

    Args:
        coin (str): Crypto name.
        currency (str): Currency name.

    Returns:
        dict: Dictionnary with the historical data.
    """

    coin = coin.strip("$/_*()")
    data = requests.get(f"https://api.coingecko.com/api/v3/coins/{coin}/market_chart?vs_currency={currency}&days=max&interval=daily").json()
    
    return data

@cache(ttl=15)
def cache_coin_data(coin: str):
    """
    Request basic informations for a coin and store it in cache.

    Args:
        coin (str): Crypto name.

    Returns:
        dict: Dictionnary with the coin informations.
    """

    coin = coin.strip("$/_*()")
    data = requests.get(f"https://api.coingecko.com/api/v3/coins/{coin}?localization=false&tickers=false&community_data=false&developer_data=false&sparkline=false").json()
    
    return data

def convert_timestamp(timestamp: int):
    """
    Convert a timestamp to a datetime object.

    Args:
        timestamp (int): Unix timestamp.

    Returns:
        datetime: Datetime object.
    """

    return datetime.utcfromtimestamp(timestamp/1000).strftime('%Y-%m-%d')

def to_prophet_format(data: pd.DataFrame) -> pd.DataFrame:
    """
    Convert a dataframe to the format required by Prophet.

    Args:
        data (pandas.DataFrame): Dataframe with the time series.

    Returns:
        pandas.DataFrame: Dataframe with the time series in the format required by Prophet.
    """

    data.drop(['market_caps', 'total_volumes'], axis=1, inplace=True)
    data.rename(columns={'prices': 'y', 'timestamps': 'ds'}, inplace=True)

    return data

def confirmed_crypto(name:str) -> bool:
    """
    Check if a crypto is confirmed.

    Args:
        name (str): Crypto name.

    Returns:
        bool: True if crypto is confirmed, False otherwise.
    """
    
    filters = ["_", ".", ";", "$"]

    for f in filters:
        name = name.replace(f, " ")

    if len(name.split()) > 1:
        return False
    else:
        return True
    