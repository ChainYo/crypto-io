import os
import psycopg2

from dotenv import load_dotenv
class DataBase():
    """
    Database connection class
    """

    @classmethod
    def connect(cls):
        """
        Connect to the database
        """

        # load .env file
        load_dotenv()

        # Get database credentials
        db_user = os.getenv("POSTGRES_USER")
        db_password = os.getenv("POSTGRES_PASSWORD")
        db_name = os.getenv("DB_NAME")
        db_host = os.getenv("DB_HOST")

        # Connect to database
        cls.conn = psycopg2.connect(
            dbname=db_name,
            user=db_user,
            password=db_password,
            host=db_host
        )

    @classmethod
    def close(cls):
        """
        Close the database connection
        """
        
        cls.conn.close()

    @classmethod
    def create_user(cls, username:str, password:str):
        """
        Creates a new user for the database
        """
        try:
            cls.connect()
            cur = cls.conn.cursor()
            cur.execute(f"""
                INSERT INTO users (username, password)
                VALUES (
                    '{username}', 
                    crypt('{password}', gen_salt('bf'))
                ) 
                ON CONFLICT (username) 
                DO NOTHING;
            """)
            cls.conn.commit()
            cls.close()
            return True
        except Exception as e: return e
    
    @classmethod
    def authentication(cls, username:str, password:str):
        """
        Checks if the user and password are correct
        """
        try:
            cls.connect()
            cur = cls.conn.cursor()
            cur.execute(f"""
                SELECT id FROM users
                WHERE username = '{username}'
                AND password = crypt('{password}', password);
            """)
            match = cur.fetchone()
            cls.close()
            return True if match else False
        except Exception as e: return e

    @classmethod
    def check_username(cls, username:str):
        """
        Checks if the username exists
        """
        try:
            cls.connect()
            cur = cls.conn.cursor()
            cur.execute(f"""
                SELECT username FROM users
                WHERE username = '{username}';
            """)
            match = cur.fetchone()
            cls.close()
            return True if match else False
        except Exception as e: return e

    @classmethod
    def set_default_currency(cls, username:str, currency:str):
        """
        Create or update the default currency choice of the user
        """
        try:
            cls.connect()
            cur = cls.conn.cursor()
            cur.execute(f"""
                INSERT INTO currency_default (user_id, currency_id)
                SELECT u.id, c.id
                FROM (SELECT id FROM users WHERE username LIKE '{username}') u
                , (SELECT id FROM currencies WHERE currency LIKE '{currency}') c
                ON CONFLICT (user_id) DO UPDATE SET currency_id = (SELECT id FROM currencies WHERE currency LIKE '{currency}');
            """)
            cls.conn.commit()
            cls.close()
        except Exception as e: return e

    @classmethod
    def get_default_currency(cls, username:str):
        """
        Get the default currency of the user

        Parameters
        ----------
        username : str
            Username of the user.

        Returns
        -------
        match : str
            Default currency of the user.
        """
        try:
            cls.connect()
            cur = cls.conn.cursor()
            # Select currency name with join from currencies table
            cur.execute(f"""
                SELECT id, currency FROM currency_default
                JOIN currencies ON currency_default.currency_id = currencies.id
                WHERE user_id IN (SELECT id FROM users WHERE username LIKE '{username}');
            """)
            match = cur.fetchone()
            cls.close()
            return match
        except Exception as e: return e

    @classmethod
    def add_to_user_targets(cls, username:str, coin:str):
        """
        Add a coin to the user targeted coins list.

        Parameters
        ----------
        username : str
            Username of the user.
        target : str
            Coin name to be added to the targeted coins list.
        """
        try:
            cls.connect()
            cur = cls.conn.cursor()
            cur.execute(f"""
                INSERT INTO user_target_list (user_id, coin_id)
                SELECT u.id, c.id
                FROM (SELECT id FROM users WHERE username LIKE '{username}') u
                , (SELECT id FROM coins WHERE coin LIKE '{coin}') c
                ON CONFLICT (user_id) DO UPDATE SET currency_id = (SELECT id FROM coins WHERE coin LIKE '{coin}');
            """)
            cls.conn.commit()
            cls.close()
        except Exception as e: return e

    @classmethod
    def get_user_targets(cls, username:str):
        """
        Get the user targeted coins list.

        Parameters
        ----------
        username : str
            Username of the user.

        Returns
        -------
        match : list
            List of user's targeted coins.
        """
        try:
            cls.connect()
            cur = cls.conn.cursor()
            # Select coin name with join from coins table
            cur.execute(f"""
                SELECT id, coin FROM user_target_list
                JOIN coins ON user_target_list.coin_id = coins.id
                WHERE user_id IN (SELECT id FROM users WHERE username LIKE '{username}');
            """)
            match = cur.fetchall()
            cls.close()
            return match
        except Exception as e: return e

    @classmethod
    def remove_from_user_targets(cls, username:str, coin:str):
        """
        Remove a coin from the user targeted coins list.

        Parameters
        ----------
        username : str
            Username of the user.
        target : str
            Coin name to be removed from the targeted coins list.
        """
        try:
            cls.connect()
            cur = cls.conn.cursor()
            cur.execute(f"""
                DELETE FROM user_target_list
                WHERE user_id IN (SELECT id FROM users WHERE username LIKE '{username}')
                AND coin_id IN (SELECT id FROM coins WHERE coin LIKE '{coin}');
            """)
            cls.conn.commit()
            cls.close()
        except Exception as e: return e
