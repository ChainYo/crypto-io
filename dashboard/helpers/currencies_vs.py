currencies_vs = """
    BTC: Bitcoin  
    ETH: Ethereum  
    LTC: Litecoin   
    BCH: Bitcoin Cash  
    BNB: Binance Coin   
    EOS: EOS   
    XRP: Ripple   
    XLM: Stellar  
    LINK: Chainlink   
    DOT: Polkadot  
    YFI: Yearn Finance   
    USD: US Dollars  
    AED: Dirham emirati  
    ARS: Argentina Peso  
    AUD: Austalian Dollar  
    BDT: Bangladeshi Taka   
    BHD: Bahraini Dinar   
    BMD: Bermudian Dollar    
    BRL: Brezilian Real   
    CAD: Candy Protocol    
    CHF: Swiss Franc  
    CLP: Chile Peso  
    CNY: China Yuan Renminbi  
    CZK: Czech Republic Koruna  
    DKK: Denmark Krone    
    EUR: Euros     
    GBP: Great Britain Pound    
    HKD: Hong Kong Dollar  
    HUF: Hungary Forint  
    IDR: Indonesia Rupiah   
    ILS: Israel Shekel   
    INR: India Rupee   
    JPY: Japan Yen  
    KRW: Korea (South) Won    
    KWD: Kuwaiti Dinar  
    LKR: Sri Lanka Rupee  
    MMK: Myanmar Kyat  
    MXN: Mexico Peso  
    MYR: Malaysia Ringgit  
    NGN: Nigeria Naira  
    NOK: Norway Krone  
    NZD: New Zealand Dollar  
    PHP: Philipine Peso  
    PKR: Pakistan Rupee  
    PLN: Poland Zloty  
    RUB: Russian Rouble  
    SAR: Saudi Arabia Riyal  
    SEK: Sweeden Krona  
    SGD: Singapore Dollar  
    THB: Thailand Baht  
    TRY: Turkey Lira  
    TWD: Taiwan New Dollar  
    UAH: Ukranian Hryvnia   
    VEF: Venezuela Bolivar Fuerte  
    VND: Viet Nam Dong  
    ZAR: South Africa Rand   
    XDR: FMI special   
    XAG: Silver   
    BITS: BITS  
    SATS: Baby Satoshi   
"""