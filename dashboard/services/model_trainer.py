"""
Model training
"""
import numpy as np
import pandas as pd

from sklearn.preprocessing import StandardScaler

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import LSTM, Dense, Dropout


class ModelTrainer():
    """
    Class for model training
    """
    def __init__(self, coin:str, dataset:pd.DataFrame, n_past:int = 14, n_future:int = 5):
        """
        Initialize the model trainer

        Parameters
        ----------
        coin : str
            The coin name for the model
        dataset : pd.DataFrame
            The dataset to train the model on
        n_past : int
            The number of past days to use for training
        n_future : int
            The number of future days to predict

        Returns
        -------
        None
        """
        self.coin = coin
        self.dataset = dataset
        self.n_past = n_past
        self.n_future = n_future
        self.columns = ["prices", "market_caps", "total_volumes"]
        self.model = None


    def run_pipeline(self):
        """
        Run the model training pipeline
        """
        self.trainX, self.trainY = self._preprocess(self.dataset)
        if not self.model:
            self.model = self._compile(self.trainX.shape, self.trainY.shape)
        _, self.model = self._train()
        self._save_model(f"models")


    def _preprocess(self, dataset:pd.DataFrame):
        """
        Preprocess the dataset

        Parameters
        ----------
        dataset : pd.DataFrame
            The dataset to preprocess
        
        Returns
        -------
        trainX : np.array
            The training data
        trainY : np.array
            The training labels
        """
        df_for_training = dataset[self.columns].astype(float)

        scaler = StandardScaler()
        scaler = scaler.fit(df_for_training)
        df_for_training_scaled = scaler.transform(df_for_training)

        trainX, trainY = [], []
        for i in range(self.n_past, len(df_for_training_scaled) - self.n_future +1):
            trainX.append(df_for_training_scaled[i - self.n_past:i, 0:df_for_training.shape[1]])
            trainY.append(df_for_training_scaled[i + self.n_future - 1:i + self.n_future, 0])

        return np.array(trainX), np.array(trainY)
    

    def _train(self, epochs:int = 15, batch_size:int = 16):
        """
        Train the model

        Parameters
        ----------
        epochs : int
            The number of epochs to train the model
        batch_size : int
            The batch size for training
        
        Returns
        -------
        history : dict
            The training history
        model : keras.models.Sequential
            The trained model
        """
        self.history = self.model.fit(
            self.trainX, self.trainY, 
            epochs=epochs, 
            batch_size=batch_size,
            validation_split=0.1, 
            verbose=1
        )

        return self.history, self.model


    def _compile(self, x_shape, y_shape):
        """
        Compile the model

        Parameters
        ----------
        x_shape : Tuple[int, int, int]
            The shape of the features training data
        y_shape : Tuple[int, int, int]
            The shape of the labels training data
        """
        model = Sequential()

        model.add(LSTM(64, activation='relu', input_shape=(x_shape[1], x_shape[2]), return_sequences=True))
        model.add(LSTM(32, activation='relu', return_sequences=False))
        model.add(Dropout(0.2))
        model.add(Dense(y_shape[1]))

        model.compile(optimizer='adam', loss='mse')

        return model

    
    def _save_model(self, path:str):
        """
        Save the model

        Parameters
        ----------
        path : str
            The path to save the model to
        """
        self.model.save(f"{path}/{self.coin}_model.h5")
