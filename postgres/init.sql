/* Add extension for password */
CREATE EXTENSION pgcrypto;

/* Create table for users */
CREATE TABLE IF NOT EXISTS users (
  id SERIAL PRIMARY KEY,
  username TEXT NOT NULL UNIQUE,
  password TEXT NOT NULL
);

/* Create table for currency list */
CREATE TABLE IF NOT EXISTS currencies (
  id SERIAL PRIMARY KEY,
  currency TEXT NOT NULL UNIQUE
);

/* Create table for coins */
CREATE TABLE IF NOT EXISTS coins (
  id SERIAL PRIMARY KEY,
  coin TEXT NOT NULL UNIQUE
);

/* Create table for currency default choice */
CREATE TABLE IF NOT EXISTS currency_default (
  choice_id SERIAL PRIMARY KEY,
  user_id INTEGER NOT NULL UNIQUE,
  currency_id INTEGER NOT NULL,
  CONSTRAINT user_id_fk 
    FOREIGN KEY (user_id) 
    REFERENCES users (id),
  CONSTRAINT currency_id_fk
    FOREIGN KEY (currency_id)
    REFERENCES currencies (id)
);

/* Create table for users targeted coins */
CREATE TABLE IF NOT EXISTS user_target_list (
  target_id SERIAL PRIMARY KEY,
  user_id INTEGER NOT NULL UNIQUE,
  coin_id INTEGER NOT NULL,
  CONSTRAINT user_id_fk
    FOREIGN KEY (user_id)
    REFERENCES users (id),
  CONSTRAINT coin_id_fk
    FOREIGN KEY (coin_id)
    REFERENCES coins (id)
);

/* Insert all currencies */
INSERT INTO currencies (currency)
VALUES
  ('BTC'), ('ETH'), ('LTC'), ('BCH'), ('BNB'), ('EOS'),  
  ('XRP'), ('XLM'), ('LINK'), ('DOT'), ('YFI'), ('USD'),
  ('AED'), ('ARS'), ('AUD'), ('BDT'), ('BHD'), ('BMD'),    
  ('BRL'), ('CAD'), ('CHF'), ('CLP'), ('CNY'), ('CZK'), 
  ('DKK'), ('EUR'), ('GBP'), ('HKD'), ('HUF'), ('IDR'),  
  ('ILS'), ('INR'), ('JPY'), ('KRW'), ('KWD'), ('LKR'), 
  ('MMK'), ('MXN'), ('MYR'), ('NGN'), ('NOK'), ('NZD'), 
  ('PHP'), ('PKR'), ('PLN'), ('RUB'), ('SAR'), ('SEK'),
  ('SGD'), ('THB'), ('TRY'), ('TWD'), ('UAH'), ('VEF'),
  ('VND'), ('ZAR'), ('XDR'), ('XAG'), ('BITS'), ('SATS');